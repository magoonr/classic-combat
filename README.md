# Classic-Combat

Classic Combat by mago is my submission for the Highsight Game Jam #1 (Atari Remakes!) https://itch.io/jam/highsight-1. I�ve tried to remake my favorite game mode of Atari Combat in 3D with basic AI and Godot Engine 3.0. I�ve also had plans to include a local coop mode and more difficult levels but I�ve run out of time.

It's my first Godot 3.0 project with the stable release. 

https://magodev.itch.io/classic-combat


## FEATURES ##
Features which are not included yet, but on my wishlist:
- Local Coop
- More Levels
- AI is chasing the player
- Scoreboards

## LICENSE ##
Music, as always, by Ozzed check out http://ozzed.net/music/

https://creativecommons.org/LICENSES/BY-SA/3.0/
All other stuff is released under https://creativecommons.org/licenses/by-nc/3.0/

Clone it, improve it, learn some Godot stuff, do what you want with it :)



Follow me on Twitter https://twitter.com/magodevel
