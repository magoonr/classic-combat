<?php
    $sql_server = "localhost";
    $sql_user = "testuser";
    $sql_pass = "testpw";
    $sql_dbase = "ccombat-sql";

    $con = mysqli_connect($sql_server, $sql_user, $sql_pass, $sql_dbase);

    $sql = "SELECT name, score FROM score ORDER BY score.score DESC LIMIT 0,10";
    $query = mysqli_query($con, $sql);
    
    echo "{ \"score\": [";

    while ($row = mysqli_fetch_array($query))
    {
        echo "{ \"name\": \"".$row['name']."\", \"points\" : \"".$row['score']."\"},";
    }

    echo "]}";

    mysqli_close ($con);

?>