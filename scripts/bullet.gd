extends RigidBody

var bowner = null #Node of the owner
var playerNode = null #Node of player
var tbounce = 0 #Amount of bounces off the wall


func _ready():
	get_node("Particles").set_emitting(true) #Go particles!
	playerNode = GLOBAL.gameNode.get_node("tank") #Set ref to player node


#Sets the owner of the bullet
func setOwner(new_owner):
	add_collision_exception_with(new_owner)
	bowner = new_owner


#Remove the y force from our bullet
func _integrate_forces(state):
	var lv = state.get_linear_velocity() #Get current state
	var transform = state.get_transform()
	lv.y = 0 #Remove y force
	state.set_linear_velocity(lv) #Re-set the state


#Collision detection
func _physics_process(delta):
	for bodie in get_colliding_bodies(): #Loop over collisions
		if bodie:
			#Users shot
			if bowner == null:
				if bodie.is_in_group("enemy"):
					if bodie.has_method("hit"):
						bodie.hit(tbounce)
						playerNode.shake()
						_on_Timer_timeout()
				else:
					tbounce += 1
			#Enemy shot
			else:
				if bodie.is_in_group("player"):
					if bodie.has_method("hit"):
						bodie.hit(1)
						_on_Timer_timeout()


#Remove everything
func _on_Timer_timeout():
	get_node("Particles").set_emitting(false)
	queue_free()
