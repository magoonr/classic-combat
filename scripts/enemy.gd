extends KinematicBody

#Preload the scenes we want to instance
export(PackedScene) var explosionScene = preload("res://scenes/explosion.tscn")
export(PackedScene) var bulletScene = preload("res://scenes/bullet.tscn")
export(PackedScene) var healthScene = preload("res://scenes/health.tscn")
export(PackedScene) var endscreenScene = preload("res://scenes/endscreen.tscn")

onready var root = get_parent().get_parent()
onready var player = root.get_node("tank")

const impulseMod = 4 #Shot impulse modifier

export(int, 0, 10) var difficulty = 10 #Aim ability
export(int, 0, 5) var healthpoints = 1 #How many shots needed
export(float, 0.5, 2.0) var shootDelay = 1.0 #Delay of new shot
export(bool) var isBoss = false #Boss?

var alive = true #Is enemy alive
var life = 1 #Health
var playerInRange = false #Is player in range
var prepareShoot = false #Is shot being prepared
var shoot = false #Is shot ready


#Init
func _ready():
	set_physics_process(true)
	setupEnemy()


#Physics
func _physics_process(delta):
	actionHandle()


#Setup Enemy Attributes
func setupEnemy():
	var mesh = get_node("MeshInstance")

	get_node("shootDelay").set_wait_time(shootDelay)
	life = healthpoints

	if isBoss:
		#Red
		mesh.set_surface_material(1, load("res://models/tank/enemy_d_1.material"))
		mesh.set_surface_material(3, load("res://models/tank/enemy_d_2.material"))
		mesh.set_scale(Vector3(1.4,1.0,1.4))
		get_node("CollisionShape").set_scale(Vector3(1.4,1.0,1.4))
	else:
		if difficulty > 6:
			#Elite: Yellow
			mesh.set_surface_material(1, load("res://models/tank/enemy_c_1.material"))
			mesh.set_surface_material(3, load("res://models/tank/enemy_c_2.material"))
		elif difficulty > 3:
			#Hard: Blue
			mesh.set_surface_material(1, load("res://models/tank/enemy_b_1.material"))
			mesh.set_surface_material(3, load("res://models/tank/enemy_b_2.material"))
		else:
			#Easy: Green
			#already set
			pass
			


#Perform physics
func actionHandle():
	if playerInRange and alive:
		var isVisible = visibleCheck()
		
		#Kick off shoot delay timer
		if !prepareShoot:
			if isVisible:
				prepareShoot = true
				get_node("shootDelay").start()
		
		#Now we can finally shoot
		elif shoot and GLOBAL.alive:
			if isVisible:
				var pos = get_node("MeshInstance/Position3D").get_global_transform().origin
				var impulse = -(self.get_global_transform().origin - pos) * Vector3(impulseMod,0,impulseMod)
				impulse = shotModifier(impulse)
				spawn_missile(self, pos, impulse)

			shoot = false
			prepareShoot = false


#Randomize Shot
func shotModifier(impulse):
	var rand = randi() % 10
	if rand > difficulty:
		#Miss shot; this is mathematically not perfect, but good enough -> missle is a bit faster now
		if rand % 2:
			impulse.x += 1.5
			impulse.z -= 1.5
		else:
			impulse.x -= 1.5
			impulse.z += 1.5
	return impulse


#Spawns a missile
func spawn_missile(owner, pos, impulse):
	var bullet = bulletScene.instance() #Instance our scene
	bullet.set_global_transform(Transform(Basis(),pos)) #Positioning the bullet
	bullet.apply_impulse(Vector3(), impulse) #Apply impulse on it
	bullet.setOwner(self) #Set owner to differ from players shots
	root.get_node("bullets").add_child(bullet) #Add it to the scene
	player.get_node("camera").shake(0.15, 10, 0.2) #Lets shake the cam a bit for visual feedback


#Spawns a health pack
func spawn_health():
	var health = healthScene.instance() #Instance it
	health.set_global_transform(Transform(Basis(), self.get_global_transform().origin)) #Move it
	root.get_node("levelEntities/collectable").add_child(health) #Add it to the scene


#Raycast to player
func visibleCheck():
	var space_state = get_world().get_direct_space_state() #Our space state
	#Let the raycast hit the floor.. no wait.. the player
	var result = space_state.intersect_ray( get_translation() + Vector3(0, 1, 0), player.get_translation() + Vector3(0, 1, 0), [self], 1 )
	
	#Houston we got a hit?
	if !result.empty():
		#Is it tagged as the player?
		if result.collider.is_in_group("player"):
			look_at(player.get_translation(), Vector3(0, 1, 0)) #Face the player
			rotate_y(deg2rad(90)) #Correct the models rotation
			return true #Is visible
	return false #Is not visible


#This function is called when being hit
func hit(bounce):
	if alive:
		life -= 1
		if life < 1: #Woops, guess Ill die
			alive = false #Im dead now
			var explosion = explosionScene.instance() #Instance explosion
			add_child(explosion) #Add it as a child
			playerInRange = false #Reset state
			prepareShoot = false #Reset state
			shoot = false #Reset state
			get_node("deadTimer").start() #Disapear soon

			if isBoss:
				GLOBAL.gameFinish() #Player has won!
				GLOBAL.alive = false
				var endscene = endscreenScene.instance()
				var playerHud = player.get_node("camera/hud")
				endscene.get_node("victory").show()
				endscene.get_node("gameOver").hide()
				playerHud.add_child(endscene)
				
			else:
				GLOBAL.addKill(difficulty, bounce) #Add kill to score
				self.remove_child(get_node("CollisionShape")) #Remove collider
				
				#Randomly spawn a health pack with 10% chance
				if randi() % 10 == 1:
					spawn_health()


#Player in Ranger detection
func _on_attackArea_body_entered( body ):
	if alive:
		if body.is_in_group("player"):
			#print("enter "+ get_name())
			playerInRange = true
			
			#shoot directly test
			shoot = true
			look_at(player.get_translation(), Vector3(0, 1, 0))
			rotate_y(deg2rad(90))


#Player in Ranger detection
func _on_attackArea_body_exited( body ):
	if body.is_in_group("player"):
		#print("exit "+ get_name())
		playerInRange = false
		prepareShoot = false
		shoot = false


#Remove Tank
func _on_deadTimer_timeout():
	queue_free()


#Delay switch
func _on_shootDelay_timeout():
	if alive:
		prepareShoot = false
		shoot = true
