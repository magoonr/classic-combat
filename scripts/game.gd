#TODO: this needs to be reworked with the menu :)

extends Spatial

export(PackedScene) var menuScene = preload("res://scenes/menu.tscn")
export(PackedScene) var gameScene = preload("res://scenes/level.tscn")

#onready var highScore = preload("res://scripts/highscore.gd").new()

var levelNode = null
var menuNode = null

func _ready():
#	if highScore.grabScore() == OK:
#		print(".. Sucessfull!")
#		print(GLOBAL.globalScore)
#	else:
#		print(".. Failed. Starting offline.")
	
	GLOBAL.environment = get_world().get_environment()


func _process(delta):
	if GLOBAL.getMode() == GLOBAL.GameMode.gameRunning or GLOBAL.getMode() == GLOBAL.GameMode.gameFinished:
		if Input.is_action_just_pressed("ui_cancel"):
			GLOBAL.setMode(GLOBAL.GameMode.gamePause)
			var menu = menuScene.instance()
			menu.camera()
			get_node("WorldEnvironment").add_child(menu)


