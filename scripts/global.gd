extends Node

const pointsPerKill = 5
const skillModifier = 1.2
const shotCosts = 1
const startHealth = 9

var hudNode = null
var gameNode = null
var environment = null

var score = 0
var health = startHealth
var alive = true

var globalScore = null

enum GameMode {
	idle,
	gameRunning,
	gamePause,
	gameOver,
	gameFinished
}

var gameState = GameMode.idle


func _ready():
	init()
	
func init():
	score = 0
	gameFinished = false
	health = startHealth
	alive = true

func setMode(value):
	if value == GameMode.gamePause:
		get_tree().set_pause(true) #Pause
		GLOBAL.gameNode.hide()
		GLOBAL.hudNode.hide()
		environment.dof_blur_far_enabled = true

	elif value == GameMode.gameRunning:
		get_tree().set_pause(false) #Reset pause
		GLOBAL.gameNode.show()
		GLOBAL.hudNode.show()
		environment.dof_blur_far_enabled = false
	gameState = value

func getMode():
	return gameState

func modifyHealth(value):
	health += value
	if health < 1:
		health = 0
		gameOver()
	elif health > startHealth:
		health = startHealth
	return health

func addKill(skill, bounce):
	score += (pointsPerKill + bounce + (skill * skillModifier)) * 10
	if hudNode:
		hudNode.updateGui(score)

func shot():
	score -= shotCosts
	if hudNode:
		hudNode.updateGui(score)

func gameOver():
	setMode(GameMode.gameFinished)

func gameFinish():
	setMode(GameMode.gameFinished)


func registerHUD(node):
	hudNode = node
	hudNode.updateGui(score)


