extends Spatial


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass


func _on_Area_body_entered( body ):
	#We only want to put it up, if we are not at full health
	if body.is_in_group("player") and GLOBAL.health < GLOBAL.startHealth:
		body.heal(10)
		queue_free()