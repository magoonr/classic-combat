extends Sprite3D

var segment = int(GLOBAL.startHealth/3)

func _ready():
	pass

func set(value):
	#Set the color. Animation is a quick and easy way to handle with multiple material colors
	if value > segment*2:
		get_node("AnimationPlayer").play("green")
	elif value > segment:
		get_node("AnimationPlayer").play("orange")
	else:
		get_node("AnimationPlayer").play("red")

	self.set_frame(GLOBAL.startHealth - value)
	
