extends Control

# online highscore is not yet implemented
# what works:
#  - getting json data from webserver (have a look at htdocs/ folder
# what dont work:
#  - submitting score, as crypto bindings are not yet implemented into godot


const header = ["User-Agent: classic-combat", "Accept: */*"]
const host = "localhost"
const port = 80
const key = "00"

func _ready():
	grabScore()

func storeScore(score, user):
	#It looks like that there are no crypto functions yet implemented :(
	pass

func grabScore():
	var retVal = FAILED
	var http = HTTPClient.new()

	retVal = http.connect_to_host(host, port)
	
	#Connect to server
	if retVal == OK:
		while (http.get_status() == HTTPClient.STATUS_CONNECTING or http.get_status() == HTTPClient.STATUS_RESOLVING):
			http.poll()
			print("Try to connect to the Classic Combat server..")
			OS.delay_msec(200)
	else:
		return retVal
	
	#Fetch scoreboard
	if (http.get_status() == HTTPClient.STATUS_CONNECTED):
		if (http.request(HTTPClient.METHOD_GET, "/ccombat/score.php", header) == OK):
			while (http.get_status() == HTTPClient.STATUS_REQUESTING):
				http.poll()
				OS.delay_msec(100)
				
			if (http.get_status() == HTTPClient.STATUS_BODY or http.get_status() == HTTPClient.STATUS_CONNECTED):
				if (http.has_response()):
					retVal = parseHtml(http)
	return retVal
	
func parseHtml(http):
	#Parse our score
	var buffer = http.read_response_body_chunk().get_string_from_ascii()
	var dict = parse_json(buffer)

	if (dict.has("score")):
		GLOBAL.globalScore = dict.score
	else:
		return FAILED
	return OK