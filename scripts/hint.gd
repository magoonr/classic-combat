extends Spatial

export(Texture) var hintTexture = null

func _ready():
	if hintTexture != null:
		get_node("Sprite3D").set_texture(hintTexture)


func _on_Area_body_entered( body ):
	if body.is_in_group("player"):
		get_node("Timer").start()


func _on_Timer_timeout():
	get_node("AnimationPlayer").play("fadeOut")

func delete():
	queue_free()