extends Spatial


func _ready():
	update()

func camera():
	get_node("3d/Camera").make_current()

func update():
	if GLOBAL.getMode() == GLOBAL.GameMode.gamePause:
		get_node("2d/VBoxContainer/continue").show()
	else:
		get_node("2d/VBoxContainer/continue").hide()


func _on_continue_button_down():
	GLOBAL.setMode(GLOBAL.GameMode.gameRunning)
	queue_free() #Remove the menu


func _on_play_button_down():
	if GLOBAL.getMode() == GLOBAL.GameMode.gamePause:
		#Remove level first before adding it again
		GLOBAL.gameNode.queue_free()
		#Reset stats
		GLOBAL.init()

	var game =  get_parent().get_parent().gameScene.instance() #Instance the level
	
	GLOBAL.gameNode = game
	get_parent().add_child(game) #Add it as a child
	#Set Game State
	GLOBAL.setMode(GLOBAL.GameMode.gameRunning)

	queue_free() #Remove the menu


func _on_config_button_down():
	print("Not yet implemented")


func _on_quit_button_down():
	get_tree().quit()
