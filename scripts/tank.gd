extends Spatial

#Preload the scenes we want to instance
onready var explosionScene = preload("res://scenes/explosion.tscn")
onready var bulletScene = preload("res://scenes/bullet.tscn")
onready var endscreenScene = preload("res://scenes/endscreen.tscn")

const speed = 7 #Driving speed
const rotSpeed = 3 #Rotation speed
const impulseMod = 5 #Impulse multiplier

var velocity = Vector3() #Store the velocity
var shootReady = true #Player is ready to shoot

func _ready():
	set_physics_process(true)

func _physics_process(delta):
	if GLOBAL.alive:
		drive(delta) #Perform player input


#Player input handling
func drive(delta):
	var aim = get_node("yaw").get_transform().basis #Get rotation
	var dir = Vector3() #Position of the tank

	if Input.is_action_pressed("ui_up"):
		dir -= aim.z #Drive forward
	if Input.is_action_pressed("ui_down"):
		dir += aim.z #Drive backward
	if Input.is_action_pressed("ui_right"):
		#Apply rotation
		get_node("yaw").rotate_y(-rotSpeed*delta)
		get_node("CollisionShape").rotate_y(-rotSpeed*delta)
	if Input.is_action_pressed("ui_left"):
		#Apply rotation
		get_node("yaw").rotate_y(rotSpeed*delta)
		get_node("CollisionShape").rotate_y(rotSpeed*delta)

	dir = dir.normalized() * speed #Normalize the coordinates
	
	var nvel = velocity
	nvel = nvel.linear_interpolate(dir, 6 * delta)
	velocity = nvel
	
	velocity = move_and_slide(velocity,Vector3(0,1,0))

	if Input.is_action_pressed("ui_select") and shootReady:
		shootReady = false
		get_node("Timer").start()
		var pos = get_node("yaw/MeshInstance/Position3D").get_global_transform().origin
		var impulse = -(self.get_global_transform().origin - pos) * Vector3(impulseMod,0,impulseMod)
		spawn_missile(null, pos, impulse)

func spawn_missile(owner, pos, impulse):
	var bullet = bulletScene.instance()
	bullet.set_global_transform(Transform(Basis(),pos))
	bullet.apply_impulse(Vector3(), impulse)
	bullet.add_collision_exception_with(self)
	get_parent().get_node("bullets").add_child(bullet)
	get_node("camera").shake(0.5, 20, 0.2)
	GLOBAL.shot()

func _on_Timer_timeout():
	shootReady = true

func shake():
	get_node("camera").shake(1.8, 40, 0.2)

func heal(amount):
	get_node("healthbar").set(GLOBAL.modifyHealth(amount))
	get_node("aheal").play()

func hit(damage):
	var life = GLOBAL.modifyHealth(-1 * damage)
	if GLOBAL.alive:
		get_node("healthbar").set(life)
		get_node("camera").shake(1.8, 60, 0.2)
		get_node("aexplotion").play()
		#print(life)
		if life < 1:
			var explosion = explosionScene.instance()
			self.add_child(explosion)
			GLOBAL.alive = false
			var endscene = endscreenScene.instance()
			get_node("camera/hud").add_child(endscene)
		